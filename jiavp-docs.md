# Dokumentation jIAVP
Gültig für Version 1.0

Anmerkung:

- INT = Integer / Ganzzahl
	- im Plugin mindestens 0
- BOOL = Boolean / Wahrheitswert
	- kann `true` (wahr) oder `false` (falsch) annehmen

## 1) Mit der jIAVP-Bibliothek arbeiten
Zur Erstellung neuer Inhalte mit jIAVP kann es nötig werden, die mitgelieferten Stylesheets anzupassen.
Diese sind in SCSS geschrieben, folglich ist es in diesem Fall unumgänglich, dieses in CSS kompilieren zu lassen.
Um dies mit einer möglichst geringen Einstiegshürde möglich zut machen, wurde der Projektordner so vorbereitet, dass
verschiedene Tools zum kompilieren unterstützt werden. Das sind derzeit:

- CodeKit
	- exklusiv für macOS
	- kostenpflichtig ($34)
	- Link: https://codekitapp.com/
- NetBeans (optional mit Gulp)
	- verfügbar für Windows, macOS, Gnu/Linux
	- freie Software
	- Link: https://netbeans.org/
	- setzt Java vorraus

### CodeKit
CodeKit ist ein Begleitprogramm zur Arbeit mit jeder beliebigen IDE. Dank der begefügten 
Konfigurationsdatei `config.codekit3` ist das Programm bereits vorkonfiguriert und sofort einsetzbar.

### NetBeans
Das gesamte Projekt ist als NetBeans-Projekt angelegt und kann problemlos in NetBeans geöffnet werden.
Um SCSS in CSS zu kompilieren, muss Ruby auf dem System vorhanden sein.

**Achtung: Alte Versionen von Sass können nicht mit Autogrid arbeiten.**

**Achtung: Diese Version unterstützt keinen Autoprefixer. Browserprefixe für bspw. Internet Explorer sind daher nicht verfügbar.**

**Achtung: Die Entwicklung von Ruby-Sass wurde eingestellt. Zukünfitge, neue Sass-Features können eventuell nicht mehr damit kompiliert werden.**

Unter `Tools -> Options -> HTML/JS -> CSS Preprocessors` kann man über den angebotenen Link `Install Sass` das notwendige Tool
zum kompilieren SCSS (was eine Unterart von Sass ist) in CSS installieren.
Die korrekten Ein- & Ausgabepfade wurde bereits vorkonfiguriert.
Derzeit ist das automatische kompilieren von SCSS in CSS via NetBeans deaktiviert. Um es zu nutzen, geht man wie folgt vor:

1) Rechtsklick auf das Projekt `jIAVP` in der Projektanzeige von NetBeans (standardmäßig links)
2) Klick auf `Properties`
3) Klick auf `CSS Preprocessors`
4) Im Tab `Sass` (i.d.R. standardmäßig geöffnet) das Häkchen `Compile Sass Files on Save` aktivieren
5) Unten im Fenster auf `OK` klicken

### NetBeans mit Gulp
Das Projektmanagement Tool Gulp wurde so eingerichtet, dass es Sass automatisch kompiliert, sobald eine geänderte Datei gespeichert wird.
Die entsprechende `gulpfile` sowie alle Werkzeuge werden mitgeliefert.
Dennoch, um Gulp zu nutzen muss sowohl Gulp als auch Node.js/npm auf dem System vorhanden sein. Dazu:

- Gulp: `Tools -> Options -> HTML/JS -> Gulp` auf den Link `Install Gulp` klicken
- npm: `Tools -> Options -> HTML/JS -> Node.js` auf den Link `Install Node.js` klicken
	- **Achtung:** Darauf achten, dass auch bei `npm` der Pfad zur `npm.cmd` unter `npm Path` hinterlegt ist

Es muss lediglich nach dem öffnen von NetBeans folgendes ausgeführt werden:
1) Rechtsklick auf das Projekt `jIAVP` in der Projektanzeige von NetBeans (standardmäßig links)
2) Hovern auf `Gulp Tasks` (beim ersten nach Start der IDE werden die Tasks einen Augenblick lang geladen, währenddessen mit der Maus auf dem Reiter verweilen)
3) Klick auf `default`

## 2) Abhängigkeiten / Dependencies
Die folgenden Aufgezählten Dateien **müssen** in der Webpage eingebunden werden. Ohne diese kann die Funktionalität von

_jIAVP_ oder einzelner Features nicht garantiert werden!

- jQuery:
	- Mindestversion: 3.3.1
	- Link: https://jquery.com/
- Popper.js
	- Mindestversion: 1.0
	- Link: https://popper.js.org/
	- Achtung: Tooltip.js wird **nicht** unterstützt
- screenfull.js
	- Mindestversion: 3.3.2
	- Link: https://sindresorhus.com/screenfull.js/
- jIAVP.css

## 3) Empfohlene Ladereihenfolger der JavaScript-Dateien
1. jquery.js
2. popper.js
3. screenfull.js
4. jiavp.js

## 4) jIAVP instanzieren
Um eine neue Instanz von jIAVP zu aufzusetzen, bedarf es sehr wenig Arbeit. Die Schrittfolge im einzelnen ist:

1. Erstellung eines `<div>`-Containers mit einer eindeutigen ID
	- siehe Beispiel -\> `<div id="player"></div>`
	- **Achtung:** Jeglicher, in dieses Element händisch eingetragene Inhalt wird von jIAVP entfernt.
2. Dieses Element wird anschließend mit einem jQuery-ID-Selektor ausgewählt, jIAVP kann daran direkt angehängt werden
	- siehe Beispiel -\> `$("#player").jIAVP({});`

Fertig! Die neue Instanz ist nun einsatzbereit. Inhalte wie Videos werden im nächsten Schritt per Konfiguration zugescgaltet.
Ein Beispiel einer vollständigen HTML-Datei mit Dependencies und jIAVP-Instanz sähe bspw. so aus:


\''  \<!DOCTYPE html\>
\''  <html>
\''  <head>
\''  <title>jIAVP instanzerien</title>
\''  <link href="styles/jIAVP.css" rel="stylesheet" />
\''  </head>
\''  <body>
\''      <!--PLAYER START-->
\''  <div id="player"></div>
\''      <!--PLAYER END-->
\''
\''      <!--IMPORT JS-DEPENDENCIES-->
\''  <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
\''  <script type="text/javascript" src="bower_components/popper.js/dist/umd/popper.min.js"></script>
\''  <script type="text/javascript" src="bower_components/screenfull/dist/screenfull.min.js"></script>
\''  <script type="text/javascript" src="jiavp.js"></script>
\''      <!--JS-IMPORT END-->
\''      <!--START jIAVP-CONFIGURATION-->
\''  <script type="text/javascript">

''          $(document).ready(function(){

''              $("#player").jIAVP({});

''                  }

''          </script>
\''          <!--CONFIGURATION END-->
\''      </body>
\''      </html>

## 5) Konfiguration

Die Konfiguration besteht aus 3 Teilen:

- SCSS-Konfiguration
- nicht-repetitiver Konfiguration (in JS &amp; SCSS)
- repetitiver Konfiguration (nur in JS)


### 5.1) SCSS-Konfiguration

Die SCSS-Konfiguraiton in der Datei _jIAVP.scss_ hat nur 3 Regeln.
**Achtung:** Änderungen an der SCSS\_Konfiguration kommene erst zum Tragen, wenn die SCSS-Datei in CSS kompiliert wurde.

- $grid-columns
	- Typ: INT
	- Wirkung: Generiert Styling für Overlay-Zellen in X Spalten
- $grid-rows
	- Typ: INT
	- Wirkung: Generiert Styling für Overlay-Zellen in X Reihen
- $import\_controlbar\_default\_styles
	- Typ: BOOL
	- Wirkung: Bestimmt, ob Standardstyling für Videokontrollleiste geladen werden soll.

### 5.2) Nicht-Repetitive Konfiguration

**Achtung:** Werden keine Werte für folgende Konfigurationseinstellungen gesetzt, treten in der Bibliothek vordefinierte Standardwerte für diese Einstellungen in Kraft.

Die Einstellungen können in dem `options`-Schlüssel gesetzt werden.

\''      $(document).ready(function(){
\''          $("#player").jIAVP({
\''              options: {
\''                  //HIER DIE EINSTELLUNGEN
\''              }
\''          });
\''      }

Folgende Einstellungen sind verfügbar:

- width
	- Typ: INT
	- Wirkung: Breite des Players
- height
	- Typ: INT
	- Wirkung: Höhe des Players
- overlay\_panels
	- Typ: INT
	- Wirkung: Anzahl zu generierender Overlay-Zellen
	- **Achtung:** Diese Einstellung muss dem Produkt der in der SCSS-Konfiguration gesetzten Werte für Anzahl der Reihen und Spalten übereinstimmen!
- controls
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollleiste an oder ab.
- poster
	- Typ: BOOL
	- Wirkung: Schaltet Video-Vorschaubilder an oder ab.
- autoplay
	- Typ: BOOL
	- Wirkung: Schaltet automatisches Abspielen des Videos an oder ab.
- clipjump
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Nächster/Vorheriger Clip" an oder ab.
- cuejump
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Nächster/Vorheriger Cuepoint" an oder ab.
- ff\_rw
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Vorspulen/Zurückspulen" an oder ab.
- play\_pause
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Play/Pause" an oder ab.
- mute\_button
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Stummschalter" an oder ab.
- audiobar
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Lautstärke" an oder ab.
- seekbar
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Fortschrittsanzeige" an oder ab.
- fullscreen
	- Typ: BOOL
	- Wirkung: Schaltet Videokontrollelement "Zum Vollbild wechseln" an oder ab.

Beispiel bei allen gesetzten Optionen:

\''      options :
\''          {
\''              width           :   640,
\''              height          :   360,
\''              overlay\_panels              :   9,
\''              controls                    :   true,
\''              poster          :   true,
\''              autoplay                    :   true,
\''              clipjump                    :   true,
\''              cuejump         :   true,
\''              ff\_rw           :   true,
\''              play\_pause                  :   true,
\''              mute\_button                 :   true,
\''              audiobar                    :   true,
\''              seekbar         :   true,
\''              fullscreen                  :   true
\''          },

### 5.3) Repetitive Konfiguration

Repetitive Einstellungen erfolgen als nummerierte Objekte. Es können so viele Objekte erstellt werden wie benötigt.  Diese werden als Kinder der Optionen höchster Ordnung angelegt. Das sind:

- `vessels`
- `videos`
- `cuepoints`

Einstellungsobjekte werden wie folgt angelegt (Besipiel Vessels, bei Videos oder Cuepoints muss nur der Bezeichner ausgetauscht werden):

\''      vessels :
\''          {
\''              0   :   {
\''                          //HIER DIE EINSTELLUNGEN
\''                      },
\''              1   :   {
\''                          //HIER DIE EINSTELLUNGEN
\''                      },
\''              2   :   {
\''                          //HIER DIE EINSTELLUNGEN
\''                      }
\''          },

Man beachte das die Nummerierung stets bei 0 beginnt. Einstellungsobjekte werden mit einem Komma voneinander getrennt, das letzte angelegte Objekt benötigt kein Komma zum Schluss.
Da hier insgesamt drei Objekte angelegt wurden, werden vom Plugin auch unmittelbar 3 `<div>`-Elemente erstellt (insofern ein Elternelement angegeben wurde, dazu mehr im Abschnitt 'Vessels'). Bei Videos würden vom Plugin im internen Video-Kontrollobjekt 3 Videos angelegt werden, welche dann auch so abgespielt werden würden (insofern mindestens eine Videodatei angegeben wurde, dazu mehr im Abschnitt 'Videos').
Bei Cuepoints würden diese Zeitmaker ebenfalls intern angelegt werden (dazu mehr im Abschnitt 'Cuepoints').

#### 5.3.1) Vessels

Für Vessels stehen folgende Einstellungsoptionen bereit.

- parent
	- Typ: String
	- Wirkung: Bestimmt Elterncontainer
	- Anmerkung: Elterncontainer sind immer die Zellen des Overlays. Diese haben eine eindeutige ID. Je nachdem, in welchem Overlay man das Vessel primär habe möchte, gibt man hier die Overlay-ID als String an.
Diese IDs sind beginnen mit 1 durchnummeriert. Die Nummerierung erfolgt von links nach recht und geht bei Reihenende in der nächsten Reihe weiter. Um auf Nummer sicher zu gehen, kann man sich die entsprechende ID auch über den Webinspektor des jeweiligen Browsers heraussuchen.
	- **Achtung**: Es muss der führende ID-Selektor, ein Rautezeichen (`#`) vorangestellt werden. Zwischen dem Selektor und dem ID-String darf kein Leerzeichen stehen.
	- Bsp.: `'#overlay_1'`
- custom\_class
	- Typ: String
	- Wirkung: Fügt dem `<div>` des Vessels eine oder mehrere individuelle Klassen an.
	- Anmerkung: Um mehrere Klassen anzufügen, trennt man diese innerhalb der Anführungszeichen mit einem Leerzeichen
	- Bsp.:
		- Eine Klasse: `'vessel_custom_class'`
		- Mehrere Klassen: `'vessel_custom_class vessel_custom_class_2'`
- popups
	- Typ: Objekt
	- Wirkung: Generiert an das Vessel angeheftete Popups
	- **Anmerkung**: Details im Anschluss
- show
	- Typ: Objekt
	- Wirkung: Steuert die Sichtbarkeit und Aktivität des Vessels
	- **Anmerkung**: Details im Anschluss

Bsp. Vessel-Einstellungen

\''          vessels :
\''              {
\''                  0   :
\''                  {
\''                          parent          :   '#overlay\_1',
\''                          custom\_class            :   'little\_helper',
\''                          popups          :   {},
\''                          show            :   {}
\''                  },
\''              }


Popup-Einstellungen:

- content\_id
	- Typ: String
	- Wirkung: Gibt die ID des Elements an, was in das Popup übernommen werden soll
- placement
	- Typ: String
	- Optionen:
		- auto
		- left(-start / -end)
		- right(-start / -end)
		- top(-start / -end)
		- bottom(-start / -end)
	- Wirkung: Legt Position in Relation zum Vessel fest.
- event
	- Typ: String
	- Optionen: Siehe jQuery Eventdokumentation (https://api.jquery.com/category/events/)
	- Wirkung: Bestimmt, auf welche Nutzerinteraktion das Popup reagieren soll.
- modifiers
	- Typ: Objekt
	- Optionen: Siehe Popper.js Modifier-Dokumentation (https://popper.js.org/popper-documentation.html#modifiers)
	- Wirkung: Übergibt dem Popup zusätzliche Funktionen.
	- Anwendung: Modifiername als Objektschlüssel. Optionen werden in das Objekt eingesetzt.
	- Bsp.:
\''          modifiers   :
\''              {
\''                  offset  :   {
\''                          enabled :   true,
\''                          offset  :   '0,10'
\''                  }
\''              }


	 
Bsp. Popup-Einstellungen:

\''      popups  : {
\''          0   : {
\''              content\_id  :   '#test\_popup\_2',
\''              placement   :   'auto',
\''              event   :   'click',
\''              modifiers   :
\''              {
\''                  offset  :   {
\''                          enabled :   true,
\''                          offset  :   '0,10'
\''                  }
\''              }
\''          }
\''      },


Show-Einstellungen

- video
	- Typ: INT (Videos-Einstellung =\> Objektschlüssel de jeweiligen Videos)
	- Wirkung: Bestimmt, auf welchen Videoclip sich die Einstellung bezieht.
- pause
	- Typ: bool
	- Wirkung: Wird der Vessel angezeigt, stoppt die Videowiedergabe
- time
	- Typ: Objekt
	- Wirkung: Bestimmt Zeitraum, in welchem das Vessel aktiv geschaltet werden soll.
	- Einstellungen:
		- start
			- Typ: String (Format H:M:S | mit führenden Nullen, wenn 0 ist Führungsnull optional)
			- Wirkung: Ab diesem Zeitpunk ist das Vessel aktiv.
		- end
			- Typ: String (Format H:M:S | mit führenden Nullen, wenn 0 ist Führungsnull optional)
			- Wirkung: Ab diesem Zeitpunk ist das Vessel wieder inaktiv.

Bsp. Show-Einstellungen:

\''      show            :   {
\''              0   :   {
\''                      video   :   0,
\''                      time    :   {
\''                              start           :   '0:0:02',
\''                              end     :   '0:0:04'
\''                      }
\''              },
\''              1   :   {
\''                      video   :   1,
\''                      time    :   {
\''                              start           :   '0:0:05',
\''                              end     :   '0:0:07'
\''                      },
\''              pause: true,
\''              }
\''      }

#### 5.3.2) Videos

Für Videos stehen folgende Einstellungsoptionen bereit.

- index
	- Typ: INT
	- Wirkung: Ist nur für die Interne Verarbeitung wichtig.
	- **Achtung**: Die Index-Option wird auf lange Sicht abgeschafft. Zur Identifierzung (z.B. für videos-Option der Show-Einstellung) sollte der jeweilige Objekt-Schlüssel verwendet werden.
	- **Achtung**: Aus Gründen der Kompatibelität mit zukünfitgen Versionen sollte der Index gleich dem Objektschlüssel sein.
- img
	- Typ: String
	- Wirkung: Ein Pfad zu einem Coverbild. Dieses wird z.B. vor dem Videostart angezeigt und dient bei schlechter Performance als Übergang zu Folgeclips.
- mp4
	- Typ: String
	- Wirkung: Ein Pfad zu einer Videoressource im 'mp4'-Format.
- ogg
	- Typ: String
	- Wirkung: Ein Pfad zu einer Videoressource im 'ogv'-Format.
- webm
	- Typ: String
	- Wirkung: Ein Pfad zu einer Videoressource im 'webm'-Format.

Bsp. Videos-Einstellungen:

\''      videos  :
\''          {
\''                  0   :
\''                  {
\''                          index       :   0,
\''                          img         :   "videos/0/res\_0.jpg",
\''                          mp4         :   "videos/0/res\_0.mp4",
\''                          ogg         :   "videos/0/res\_0.ogv",
\''                          webm        :   "videos/0/res\_0.webm"
\''                  },
\''                  1   :
\''                  {
\''                          index       :   1,
\''                          img         :   "videos/1/res\_1.jpg",
\''                          mp4         :   "videos/1/res\_1.mp4",
\''                          ogg         :   "videos/1/res\_1.ogv",
\''                          webm        :   "videos/1/res\_1.webm"
\''                  },
\''                  2   :
\''                  {
\''                          index       :   2,
\''                          img         :   "videos/2/res\_2.jpg",
\''                          mp4         :   "videos/2/res\_2.mp4",
\''                          ogg         :   "videos/2/res\_2.ogv",
\''                          webm        :   "videos/2/res\_2.webm"
\''                  },
\''          }

#### 5.3.3) Cuepoints

Für Videos stehen folgende Einstellungsoptionen bereit.

- video
	- Typ: INT (Videos-Einstellung =\> Objektschlüssel de jeweiligen Videos)
	- Wirkung: Bestimmt, in welchem Video der Cuepoint gesetzt werden soll.
- time
	- Typ: String (Format H:M:S | mit führenden Nullen, wenn 0 ist Führungsnull optional)
	- Wirkung: Bestimmt Zeitpunkt des Cuepoints

Bsp. Cuepoints-Einstellungen:

\''      cuepoints   :
\''          {
\''                  0   :
\''                  {
\''                      video: 0,
\''                      time: '{0:0:05}'
\''                  },
\''                  1   :
\''                  {
\''                      video: 2,
\''                      time: '{0:04:02}'
\''                  },
\''                  2   :
\''                  {
\''                      video: 3,
\''                      time: '{0:1:15}'
\''                  },
\''          }

## 6) Nutzung von ClipJump
Mit dem ClipJump-Feature ist es möglich, zu jedem beliebigen Zeitpunkt zu einem bestimmten Videoclip zu springen. Ein ClipJump wird mit dem clipJump-Event ausgelöst, wobei die ID des Zielclips mit übergeben werden muss. Das clipJump-Event selbst muss wiederum über ein anderes Event, bspw. einen Click ausgelöst werden. Die entsprechenden Script-Zeilen \*\* müssen nach\*\* der jIAVP-Konfiguration in den Seitenquelltext geschrieben werden, da dem Browser das clipJump-Event sonst nicht bekannt ist.

Wenn man bspw. einen Button via Vessel oder Popup während Clip #3 einblendet, welcher auf Klick Clip #7 starten soll, müsste man folgendes Skript in den Seitenquelltext schreiben:
\''$('#mein\_button').on('click', function(){
\''     $(this).trigger('clipJump', 7);
\'' });

## 6.5) Weitere öffentlich Events

- updateTime:
	- hiermit ist es möglich, den aktuellen Zeitpunkt im Video zur Laufzeit zu ändern
	- das funktioniert nur im aktuellen Clip!
	- das Event übernimmt den Zeitpunkt in Angabe von Sekunden als String oder Integer
	- Beispiel:
\’’$('#mein\_button').on('click', function(){
\''     $(this).trigger('updateTime ' , 12);
\'' });
- continue:
	- dieses Event ermöglicht es, mittel einem Interaktiven Element die Videowiedergabe fortzusetzen, wenn sie durch das ‚pause‘-Attribut angehalten wurde
	- dieses Event hat keine Übergabeparameter
	- Beispiel:
\’’$('#mein\_button').on('click', function(){
\''     $(this).trigger('continue');
\'' });
## 7) Beispiel einer vollständigen Website mit jIAVP-Player

\''      \<!DOCTYPE html\>
\''      <html>
\''      <head>
\''              <title>jIAVP Test</title>
\''              <link href="styles/reset-min.css" rel="stylesheet" />
\''              <link href="styles/jIAVP.css" rel="stylesheet" />
\''              <style>
\''                      div#overlay\_1{
\''                              background-color: tomato;
\''                      }
\''                      div.little\_helper{
\''                              height: 17%;
\''                              width: 10%;
\''                              background-color: #498f07;
\''                              top: 43%;
\''                              left: 45%;
\''                              border-radius: 50%;
\''                      }
\''              </style>
\''  
\''      </head>
\''      <body>
\''              <div id="player"></div>
\''              <div id="test_popup_1"><button id="testbutton">Clip 3</button></div>
\''              <div id="test_popup_2"><span>TEST 2</span></div>
\''  
\''      <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
\''      <script type="text/javascript" src="bower_components/popper.js/dist/umd/popper.min.js"></script>
\''      <script type="text/javascript" src="bower_components/screenfull/dist/screenfull.min.js"></script>
\''      <script type="text/javascript" src="jiavp.js"></script>
\''      <script type="text/javascript">

''          $(document).ready(function(){

''              $("#player").jIAVP({

''                  options :

''                      {

''                          width           :   640,

''                          height          :   360,

''                          overlay_panels          :   9,

''                          controls        :   true,

''                          poster          :   true,

''                          autoplay        :   true,

''                          clipjump        :   true,

''                          cuejump         :   true,

''                          ff_rw           :   true,

''                          play_pause      :   true,

''                          mute_button     :   true,

''                          audiobar        :   true,

''                          seekbar         :   true,

''                          fullscreen      :   true

''                      },

''                  vessels :

''                      {

''                          0   :

''                          {

''                              parent          :   '#overlay_1',

''                              custom_class            :   'little_helper',

''                              popups          :   {

''                                  0   :   {

''                                      content_id  :   '#test_popup_1',

''                                      placement   :   'top',

''                                      event       :   'click',

''  

''                                  }

''                              },

''                              show            :   {

''                                  0   :   {

''                                      video   :   0,

''                                      time    :   {

''                                          start           :   '0:0:02',

''                                          end     :   '0:0:04'

''                                      }

''                                  },

''                                  1   :   {

''                                      video   :   1,

''                                      time    :   {

''                                          start           :   '0:0:05',

''                                          end     :   '0:0:07'

''                                      }

''                                  }

''                              }

''                          },

''                          1   :

''                          {

''                              parent          :   '#overlay_2',

''                              custom_class            :   'little_helper',

''                              popups          :   {

''                                  0   :   {

''                                      content_id  :   '#test_popup_2',

''                                      placement   :   'auto',

''                                      modifiers   :

''                                      {

''                                          offset  :   {

''                                              enabled :   true,

''                                              offset  :   '0,10'

''                                          }

''                                      }

''                                  }

''                              },

''                              show            :   {

''                                  0   :   {

''                                      video   :   1,

''                                      time    :   {

''                                          start   :   '0:0:02',

''                                          end     :   '0:0:04'

''                                      }

''                                  },

''                                  1   :   {

''                                      video   :   1,

''                                      time    :   {

''                                          start   :   '0:0:05',

''                                          end     :   '0:0:07'

''                                      }

''                                  }

''                              }

''                          }

''                      },

''                  videos  :

''                      {

''                          0   :

''                          {

''                              index       :   0,

''                              img         :   "videos/0/res_0.jpg",

''                              mp4         :   "videos/0/res_0.mp4",

''                              ogg         :   "videos/0/res_0.ogv",

''                              webm        :   "videos/0/res_0.webm"

''                          },

''                          1   :

''                          {

''                              index       :   1,

''                              img         :   "videos/1/res_1.jpg",

''                              mp4         :   "videos/1/res_1.mp4",

''                              ogg         :   "videos/1/res_1.ogv",

''                              webm        :   "videos/1/res_1.webm"

''                          },

''                          2   :

''                          {

''                              index       :   2,

''                              img         :   "videos/2/res_2.jpg",

''                              mp4         :   "videos/2/res_2.mp4",

''                              ogg         :   "videos/2/res_2.ogv",

''                              webm        :   "videos/2/res_2.webm"

''                          },

''                          3   :

''                          {

''                              index       :   3,

''                      //      ''  img         :   "videos/3/res_3.jpg",

''                              mp4         :   "videos/3/res_3.mp4",

''                              ogg         :   "videos/3/res_3.ogv",

''                              webm        :   "videos/3/res_3.webm"

''                          },

''                          4   :

''                          {

''                              index       :   4,

''                          //      img         :   "videos/4/res_4.jpg",

''                              mp4         :   "videos/4/res_4.mp4",

''                              ogg         :   "videos/4/res_4.ogv",

''                              webm        :   "videos/4/res_4.webm"

''                          }

''                      },

''              cuepoints   :

''                  {

''                      0   :

''                      {

''                          video: 0,

''                          time: '{0:0:05}'

''                      },

''                      1   :

''                      {

''                          video: 2,

''                          time: '{0:04:02}'

''                      },

''                      2   :

''                      {

''                          video: 3,

''                          time: '{0:1:15}'

''                      },

''                  }

''  

''  

''              });

''          });

''  

''  

''      $('#testbutton').on('click', function(){

''          $(this).trigger('clipJump', 3);

''      });

''  

''  

''      </script>
\''  
\''  
\''      </body>
\''      </html>