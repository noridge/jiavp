(function($){

	$.fn.jIAVP = function(params){
			
		// MAKE CERTAIN VALUES ACCESSIBLE FROM THE OUTSIDE
		$.fn.jIAVP.getCurrentVideo = function(){
			return currentVideo;
		}

		$.fn.jIAVP.getContainerIDs = function(){
			return id;
		}

		// SUPPORTING FUNCTIONS
		function unique_id() {
		  function id_string() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		  }
		  return id_string() + id_string() + '-' + id_string() + '-' + id_string() + '-' + id_string() + '-' + id_string() + id_string() + id_string();
		}

		function control_width(my_toggle, init){
			if(my_toggle === true){
					$('#'+id['controls'] + '_progress').width(init);
				}
			setTimeout(function(){
				var full_width 		= $('#'+id['container']).width();
				var element_width 	= 0;


				$.each($('.player_control'), function(){
					element_width += $(this).width();
				});
				element_width += element_width/110;
				$('#'+id['controls'] + '_progress').width(full_width - element_width);
			}, 200);
		}
		
		
		
		function endPlayback(){
			$('#'+id['player']).empty();
			$('#'+id['audio']).empty();
			document.getElementById(id['player']).load();
			document.getElementById(id['audio']).load();
		}
		
		
		function appendSources(targetId, types, media = 'videos') {
			let access = settings[media][targetId];
			let mediatype = media.replace(/s$/, '');
			let parent;

			$(document).trigger('jiavp_src_update', {
				type:	mediatype,
				id	:	targetId
			});
			
			if(media == 'videos'){
				parent = $('#'+id['player']);
				if(settings['options']['poster'] === true && isDefined(settings['videos'][targetId]['img'])){
					parent.attr('poster', settings['videos'][targetId]['img']);
				}
			}else{
				parent = $('#'+id['audio']);
			}
			
			
			parent.empty();
						
			if(!Array.isArray(types)){
				types = [types];
			}
			
			for(type of types) {
				if(isDefined(access[type])){
					let srcPath = `${settings[media][targetId][type]}`;
					parent.append(`<source src="${srcPath}" type="${mediatype}/${type}" />`);
				}
			}
		}
		
		function manageAdditionalAudioTrack(videoId){
			if (isDefined(settings['audio'])){
				let volume = (typeof last_volume !== 'undefined' && last_volume !== null ? last_volume : 1);
				for(let trackId in settings['audio']){
					let track = settings['audio'][trackId];
					if(Array.isArray(track['video'])){
						// audiotrack is supposed to play over multiple clips
						
						// is this the first video? then add audio
						if(videoId == track['video'][0]){
							$('#'+id['audio']).get(0).volume = volume;
							$('#'+id['audio']).data('index',track['index']);
							appendSources(track['index'], ['mp3'], 'audio');
							
							if(isDefined(track['start'])){
								let splitStart 		= 	track['start'].split(':');
								let startSeconds	=	(+splitStart[0]) * 60 * 60 + (+splitStart[1]) * 60 + (+splitStart[2]);
								$('#'+id['audio']).get(0).currentTime = startSeconds;
							}
							
							return;
						}
						
						// if video id is in array, do nothing
						if(track['video'].includes(videoId)){
							return;
						}
						
					}else if(videoId == track['video']){
						// audiotrack is supposed to play only on this clip
						$('#'+id['audio']).get(0).volume = volume;
						$('#'+id['audio']).data('index',track['index']);
						appendSources(track['index'], ['mp3'], 'audio');
						
						if(isDefined(track['start'])){
							let splitStart 		= 	track['start'].split(':');
							let startSeconds	=	(+splitStart[0]) * 60 * 60 + (+splitStart[1]) * 60 + (+splitStart[2]);
							$('#'+id['audio']).get(0).currentTime = startSeconds;
						}
						return;
					}
				}
				// no audiotrack for this video
				if ($('#'+id['audio']).get(0).duration > 0 && !$('#'+id['audio']).get(0).paused){
					let index = $('#'+id['audio']).data().index;
					if(settings['audio'][index]['fade']){
						$('#'+id['audio']).animate({volume: 0}, 5000, () => {
							$('#'+id['audio']).get(0).pause();
							$('#'+id['audio']).empty();
						});
					}else{
						$('#'+id['audio']).get(0).pause();
						$('#'+id['audio']).empty();
					}
				}
			}
		}
		
		function videoHasAdditionalAudio(videoId){
			if(isDefined(settings['audio'])){
				var result = false;
				for(let trackId in settings['audio']){
					let track = settings['audio'][trackId];
					if(Array.isArray(track['video'])){
						if(track['video'].includes(videoId)){
							result = true;
						}
					}else if(videoId == track['video']){
						result =  true
						
					}
				}
				return result;
			}
		}
		
		function isDefined(param) {
			return typeof param !== "undefined" && param !== (null || "");
		}
		
		function createTemp(){
			let temp_id = "temp_"+unique_id();
			$('#'+id['container']).append(`<div id="${temp_id}" class="temp_container"></div>`);
			temp.push(temp_id);
			return temp_id;
		}
		
		function destroyAllTemp(){
			$(document).trigger('ts_abort');
			temp.forEach(el => {
				$('#' + el).remove();
			});
		}
		
		function removeInteractiveClipElements(){
			$('.' + class_['popup']).hide();
			
			// only for 'following the sun'-project
			$('.sculpture_popup').hide();
		}

		
		// DEFINE ACCESS PROPERTIES

		const id = {
			container	:	"container_"+unique_id(),
			player		:	"player_"+unique_id(),
			controls	:	"controls_"+unique_id(),
			audio		:	"audio_"+unique_id(),
			startbutton	:	"startbutton_"+unique_id()
		};
		
		const class_ = {
			popup		:	"popup_"+unique_id()
		};
		
		var temp = [];

// 		SET SETTINGS-VAR AND SET DEFAULTS

		const settings = $.extend({
			options : {
				width			:	640,
				height			:	'auto',
				overlay_panels 	: 	9,
				controls		:	true,
				poster			:	true,
				autoplay		:	false,
				clipjump		:	false,
				cuejump			:	false,
				ff_rw			:	false,
				play_pause		:	true,
				mute_button		:	true,
				audiobar		:	true,
				seekbar			:	true,
				fullscreen		:	true,
				startbutton		:	false
			},
			effects : {
				fade : false
			}
		}, params || {});

		var currentVideo	=	0;

// 		CREATE VTT-EVENT (VESSEL-TIME-TOGGLE) TO TRIGGER VESSELS SHOW/HIDE STATUS
		$.Event('jiavp_vtt');
// 		CREATE POPUP-WATCHER-EVENT TO REMOTELY DESTROY POPUPS IF NECESSARY
		$.Event('jiavp_pw');
//		CREATE EVENT TO TRIGGER CLIPJUMP
		$.Event('clipJump');
//		CREATE EVENT TO CONTINUE PLAYING
		$.Event('continue');
//		CREATE EVENT TO UPDATE TIME
		$.Event('updateTime');
//		CREATE EVENT TO PAUSE PLAYBACK
		$.Event('jiavp_pause');
//		CREATE EVENT TO CHECK SOURCE FILES CHANGE
		$.Event('jiavp_src_update');

//		CONTINUE-EVENT
		$(document).on('continue', function(){
			playing = true;
			$('#' + id['controls'] + '_play_pause').addClass('icon-play-solid').removeClass('icon-pause-solid');
			$('#'+id['player']).get(0).play();
			if(videoHasAdditionalAudio(currentVideo)){
				$('#'+id['audio']).get(0).play();
			}
		});
//		PAUSE-EVENT
		$(document).on('jiavp_pause', function(){
			playing = false;
			$('#'+id['player']).get(0).pause();
			$('#'+id['audio']).get(0).pause();
		});

		$(document).on('updateTime', function(evt, time){
			$('#'+id['player']).get(0).currentTime = parseInt(time);
			if(videoHasAdditionalAudio(currentVideo)){
				$('#'+id['audio']).get(0).currentTime = parseInt(time);
			}
		});


		$(document).on('clipJump', function(evt, targetClip){
			if(targetClip <= Object.keys(settings['videos']).length - 1){
				endPlayback();
				destroyAllTemp();
				if(isDefined(settings['videos'][targetClip]['func'])){
					let func = settings['videos'][targetClip]['func'];
					let hold = createTemp();
					$('#'+hold)[func[0]](...func[1]);
				}else{
					appendSources(targetClip, ['mp4', 'ogg', 'webm']);
				}
				currentVideo = targetClip;
				removeInteractiveClipElements();
				manageAdditionalAudioTrack(currentVideo);
				$('#'+id['player']).get(0).load();
				if(playing === true){
					$('#'+id['player']).get(0).play();
					if(videoHasAdditionalAudio(currentVideo)){
						$('#'+id['audio']).get(0).play();
					}
					$('#' + id['controls'] + '_play_pause').addClass('icon-pause-solid').removeClass('icon-play-solid');
				}else if(playing === false){
					$('#'+id['player']).get(0).pause();
					$('#'+id['audio']).get(0).pause();
					$('#' + id['controls'] + '_play_pause').addClass('icon-play-solid').removeClass('icon-pause-solid');
				}
				if(typeof settings['videos'][targetClip]['img'] == "undefined"){
					$('#'+id['player']).removeAttr('poster');
				}
			}
		});
		
		
		// make clipchange castable from out there
		$(document).on('nextClip', function(e, index){
			//console.log('nextClip = cur('+currentVideo+') & index('+index+')');
			if(currentVideo == index){
				$('#'+id['player']).trigger('ended');
			}
		});


		this.html('<div id="' + id['container'] + '" class="jiavp_container"></div>');
		$('#'+id['container']).height(settings['options']['height']);
		$('#'+id['container']).width(settings['options']['width']);


// 		CREATE OVERLAY-CELLS
		for(var i = 1; i < settings['options']['overlay_panels']+1; i++){
			$('#'+id['container']).append(`<div id="overlay_${i}" class="overlay"></div>`);
		}


// 		CREATE VIDEO-CONTROLBAR
		$('#'+id['container']).append('<video id="' + id['player'] + '" class="jiavp_player z0" ' + (settings['options']['autoplay'] ? "autoplay" : "") +" "+ (settings['options']['poster'] && isDefined(settings['videos'][0]['img']) ? "poster='" + settings['videos'][0]['img'] + "'" : "") +'></video>');
		

		$('#'+id['container']).append('<audio id="' + id['audio'] + '" style="display:hidden;" ' + (settings['options']['autoplay'] ? "autoplay" : "") + '></audio>');
		manageAdditionalAudioTrack(0);
		
		
		if((settings['options']['controls'] === true) && ((settings['options']['clipjump'] === true) || (settings['options']['cuejump'] === true) || (settings['options']['ff_rw'] === true) || (settings['options']['play_pause'] === true) || (settings['options']['mute_button'] === true) || (settings['options']['audiobar'] === true) || (settings['options']['seekbar'] === true) || (settings['options']['fullscreen'] === true))){
			$('#'+id['container']).append(
					'<div id="' + id['controls'] + '_parent" class="player_control_parent">'
				+	(settings['options']['clipjump']		?	'<span 	id="' + id['controls'] + '_prev_clip" 	class="player_control icon-fast-backward-solid"></span>': "")
				+	(settings['options']['cuejump'] 		?	'<span 	id="' + id['controls'] + '_prev_cue" 	class="player_control icon-step-backward-solid"></span>': "")
				+	(settings['options']['ff_rw'] 			?	'<span 	id="' + id['controls'] + '_back" 		class="player_control icon-backward-solid"></span>'		: "")
				+	(settings['options']['play_pause'] 		?	'<span 	id="' + id['controls'] + '_play_pause" 	class="player_control icon-pause-solid"></span>'		: "")
// 				+	'	<span 	id="' + id['controls'] + '_stop" 			class="player_control icon-stop2"></span>'
				+	(settings['options']['ff_rw'] 			?	'<span 	id="' + id['controls'] + '_for" 		class="player_control icon-forward-solid"></span>'		: "")
				+	(settings['options']['cuejump'] 		?	'<span 	id="' + id['controls'] + '_next_cue" 	class="player_control icon-step-forward-solid"></span>'	: "")
				+	(settings['options']['clipjump'] 		?	'<span 	id="' + id['controls'] + '_next_clip"	class="player_control icon-fast-forward-solid"></span>'	: "")
				+	(settings['options']['seekbar'] 		?	'<input id="' + id['controls'] + '_progress" 	class="player_control" type="range" value="0" />'		: "")
				+	(settings['options']['mute_button'] 	?	'<span 	id="' + id['controls'] + '_audio" 		class="player_control icon-volume-solid"></span>'		: "")
				+	(settings['options']['audiobar'] 		?	'<input id="' + id['controls'] + '_audiobar" 	class="player_control" type="range" min="0" max="1" step="0.1" value="0.5" />' : "")
				+	(settings['options']['fullscreen'] 		?	'<span 	id="' + id['controls'] + '_fullscreen" 	class="player_control icon-expand-alt-solid"></span>'	: "")
				+	'</div>'
			);
			var initial_range_width	= $('#'+id['controls'] + '_progress').width();
			control_width(false, initial_range_width);


		}
		
//		CONTROLS FADE IN AND OUT DEPENDING ON MOUSEMOVEMENT

		var xTimer;
		$('#'+id['container']).on('mousemove', function(){
			clearInterval(xTimer);
			$('#' + id['controls'] + '_parent').fadeIn(500);
			xTimer = setTimeout(function(){
				$('#' + id['controls'] + '_parent').fadeOut(500);   
			}, 2000);  
		}).on('mouseleave', function(){
			clearInterval(xTimer);
			xTimer = setTimeout(function(){
				$('#' + id['controls'] + '_parent').fadeOut(500);   
			}, 2000); 
		});


// 		ADD FIRST VIDEO OR RUN FUNC ON VIDEO CONTAINER
		
		if(isDefined(settings['videos'][0]['func'])){
			let func = settings['videos'][0]['func'];
			//old on all ids
			//$('#'+id[func[0]])[func[1]](...func[2]);
			let hold = createTemp();
			$('#'+hold)[func[0]](...func[1]);
		}else{
			appendSources(0, ['mp4', 'ogg', 'webm']);
		}


// 		SET NEXT VIDEO WHEN PREVIOUS ENDED

		$('#'+id['player']).on('ended', function(){
			//console.log('ended: ' + currentVideo);
			if(settings['effects']['fade']){
				$('#'+id['player']).fadeOut();
			}
			if(currentVideo < Object.keys(settings['videos']).length - 1){
				if(isDefined(settings['videos'][currentVideo + 1]['func'])){
					endPlayback();
					let func = settings['videos'][currentVideo + 1]['func'];
					let hold = createTemp();
					$('#'+hold)[func[0]](...func[1]);
				}else{
					appendSources(currentVideo + 1, ['mp4', 'ogg', 'webm']);
					$('#'+id['player']).get(0).load();
					$('#'+id['player']).get(0).play();
					destroyAllTemp();
				}
				removeInteractiveClipElements();

				currentVideo += 1;
				
				manageAdditionalAudioTrack(currentVideo);
				if(videoHasAdditionalAudio(currentVideo)){
					$('#'+id['audio']).get(0).play();
				}
				
				if(settings['effects']['fade']){
					$('#'+id['player']).fadeIn();
					if(videoHasAdditionalAudio(currentVideo) && $('#'+id['audio']).get(0).duration <= 0 && !mute){
						$('#'+id['audio']).animate({volume: last_volume}, 1000);
					}
				}
			}else{
				$('#'+id['player']).get(0).pause();
				$('#'+id['audio']).get(0).pause();
			}



		});


		$('#'+id['player']).on('play',function(){
			$('.vessel').trigger('jiavp_vtt');
		});


// 		ADD VESSELS TO DOM
		$.each(settings['vessels'], function(index){
			$(this['parent']).append('<div id="vessel_'+index+'" class="vessel' + (typeof this["custom_class"] !== "undefined" ? " " + this["custom_class"] : "") + '"></div>');
			$('#vessel_'+index).hide();
			
			if(isDefined(this['content_id'])){
				let content		=	$(this['content_id']);
				content.detach();
				content.appendTo($('#vessel_'+index));
			}

// 			ENABLE POPUPS
			if(isDefined(this['popups'])){
				if(this['popups'] !== '{}'){
					$.each(this['popups'], function(){
						let ref 		= 	$('#vessel_'+index);
						let popup		=	$(this['content_id']);
						let position 	= 	this['placement'];
						let modifiers	=	this['modifiers'];
						popup.detach();
						popup.appendTo($('#'+id['container']));
						popup.addClass(class_['popup']);
						popup.hide();
						ref.on((typeof this['event'] !== 'undefinded' ? this['event'] : 'click'), function(){
 							popup.toggle();
							Popper.createPopper(ref, popup, {
								placement	:	position,
								modifiers	:	(modifiers ? modifiers : ''),
								onCreate	:	function(data){
									if(screenfull.enabled) {
										screenfull.on('change', function(){
											data.instance.update();
										});
									}
									ref.on('jiavp_pw', function(){
										data.instance.destroy();
										popup.hide();
									});

								}
							});
						});
					});
				}
			}

// 			VESSEL TIMED TOGGLE
			if(isDefined(this['show'])){
				$.each(this['show'], function(){
					var stopped_once	=	false;
 					$('#vessel_'+index).on('jiavp_vtt', {video:this['video'],time:{start:this['time']['start'],end:this['time']['end']},pause:this['pause']}, function(e){
	 					var data 			= 	e.data;

	 					var splitStart 		= 	data.time.start.split(':');
	 					var startSeconds	=	(+splitStart[0]) * 60 * 60 + (+splitStart[1]) * 60 + (+splitStart[2]);

	 					var splitEnd		=	data.time.end.split(':');
	 					var endSeconds		=	(+splitEnd[0]) * 60 * 60 + (+splitEnd[1]) * 60 + (+splitEnd[2]);

	 					var interval 		= 	setInterval(checkTime, 100);

	 					var stop			=	(typeof data.pause !== "undefined" ? (data.pause ? data.pause : false) : false);

	 					function checkTime(){
		 					if(Math.floor($('#'+id['player']).get(0).currentTime) == startSeconds){
			 					if(currentVideo === data.video){
				 					$('#vessel_'+index).show();
				 					if(stop && !stopped_once){
					 					$('#'+id['player']).get(0).pause();
					 					var playing = false;
					 					stopped_once = true;
				 					}
			 					}
		 					}else if(Math.floor($('#'+id['player']).get(0).currentTime) == endSeconds){
			 					$('#vessel_'+index).trigger('jiavp_pw').hide();
		 					}
// 		 					clearInterval(interval);
	 					}
	 					if(currentVideo !== data.video){
		 					$('#vessel_'+index).trigger('jiavp_pw').hide();
	 					}
 					});
				});
			}
		});


// 		CONTROLS
// 		PLAY/PAUSE
		if(settings['options']['autoplay'] == false){
			var playing = false;
			$('#'+id['controls'] + '_play_pause').addClass('icon-play-solid');
		}else if(settings['options']['autoplay'] == true){
			var playing = true;
			$('#'+id['controls'] + '_play_pause').addClass('icon-pause-solid');
		}

		$('#'+id['controls'] + '_play_pause').on('click', function(){
			if(playing === false){
				$('#'+id['player']).get(0).play();
				if(videoHasAdditionalAudio(currentVideo)){
					$('#'+id['audio']).get(0).play();
				}
				$('#' + id['controls'] + '_play_pause').addClass('icon-pause-solid').removeClass('icon-play-solid');
				playing = true;
			}else if(playing === true){
				$('#'+id['player']).get(0).pause();
				$('#'+id['audio']).get(0).pause();
				$('#' + id['controls'] + '_play_pause').addClass('icon-play-solid').removeClass('icon-pause-solid');
				playing = false;
			}
		});
// 		PREV/NEXT VIDEO IN ROW
		const prevButton 	=	$('#'+id['controls'] + '_prev_clip');
		const nextButton 	=	$('#'+id['controls'] + '_next_clip');
		const bar 			=	$('#' + id['controls'] + '_progress');

		prevButton.on('click', function(){
			$('#'+id['player']).get(0).pause();
			if(currentVideo > 0){
				
				if(isDefined(settings['videos'][currentVideo - 1]['func'])){
					endPlayback();
					let func = settings['videos'][currentVideo - 1]['func'];
					let hold = createTemp();
					$('#'+hold)[func[0]](...func[1]);
				}else{
					appendSources(currentVideo - 1, ['mp4', 'ogg', 'webm']);
					destroyAllTemp();
				}
				removeInteractiveClipElements();
				currentVideo -= 1;
				manageAdditionalAudioTrack(currentVideo);
				bar.val(0);
			}
			$('#'+id['player']).get(0).load();
			if(playing === true){
				$('#'+id['player']).get(0).play();
				if(videoHasAdditionalAudio(currentVideo)){
					$('#'+id['audio']).get(0).play();
				}
				$('#' + id['controls'] + '_play_pause').addClass('icon-pause-solid').removeClass('icon-play-solid');
			}else if(playing === false){
				$('#'+id['player']).get(0).pause();
				$('#'+id['audio']).get(0).pause();
				$('#' + id['controls'] + '_play_pause').addClass('icon-play-solid').removeClass('icon-pause-solid');
			}

			//USE HERE currentVideo VAR AS IT ALREADY HAS BEEN UPDATED TO THE UPCOMMING VIDEO
			if(typeof settings['videos'][currentVideo]['img'] == "undefined"){
				$('#'+id['player']).removeAttr('poster');
			}
		});

		nextButton.on('click', function(){
			$('#'+id['player']).get(0).pause();
			if(currentVideo < Object.keys(settings['videos']).length - 1){
				
				if(isDefined(settings['videos'][currentVideo + 1]['func'])){
					endPlayback();
					let func = settings['videos'][currentVideo + 1]['func'];
					let hold = createTemp();
					$('#'+hold)[func[0]](...func[1]);
				}else{
					appendSources(currentVideo + 1, ['mp4', 'ogg', 'webm']);
					destroyAllTemp();
				}
				removeInteractiveClipElements();

				currentVideo += 1;
				manageAdditionalAudioTrack(currentVideo);
				bar.val(0);
			}
			$('#'+id['player']).get(0).load();
			if(playing === true){
				$('#'+id['player']).get(0).play();
				if(videoHasAdditionalAudio(currentVideo)){
					$('#'+id['audio']).get(0).play();
				}
				$('#' + id['controls'] + '_play_pause').addClass('icon-pause-solid').removeClass('icon-play-solid');
			}else if(playing === false){
				$('#'+id['player']).get(0).pause();
				$('#'+id['audio']).get(0).pause();
				$('#' + id['controls'] + '_play_pause').addClass('icon-play-solid').removeClass('icon-pause-solid');
			}

			//USE HERE currentVideo VAR AS IT ALREADY HAS BEEN UPDATED TO THE UPCOMMING VIDEO
			if(typeof settings['videos'][currentVideo]['img'] == "undefined"){
				$('#'+id['player']).removeAttr('poster');
			}
		});


// 		VIDEO-PROGRESSBAR

		bar.on('change', function(){
			var update_time = $('#'+id['player']).get(0).duration * (bar.val() / 100);
			$('#'+id['player']).get(0).currentTime = update_time;
		});
		$('#'+id['player']).on('timeupdate', function(){
			var update_progress = (100 / $('#'+id['player']).get(0).duration) * $('#'+id['player']).get(0).currentTime;
			bar.val(update_progress);
		});
		bar.on('mousedown', function(){
			$('#'+id['player']).get(0).pause();
			$('#'+id['audio']).get(0).pause();
		});
		bar.on('mouseup', function(){
			if(playing === true){
				$('#'+id['player']).get(0).play();
				if(videoHasAdditionalAudio(currentVideo)){
					$('#'+id['audio']).get(0).play();
				}
			}
		});

// 		AUDIO
		var mute = false;
		var last_volume = null;
		$('#' + id['controls'] + '_audio').on('click', function(){
			if(mute == false){
				$('#' + id['controls'] + '_audio').addClass('icon-volume-mute-solid').removeClass('icon-volume-stash-solid icon-volume-down-solid icon-volume-solid icon-volume-up-solid');
				last_volume = $('#' + id['controls'] + '_audiobar').val();
				$('#' + id['controls'] + '_audiobar').val(0);
				$('#'+id['player']).attr('muted');
				$('#'+id['audio']).attr('muted');
				mute = true;
			}else if(mute == true){
				$('#' + id['controls'] + '_audiobar').val(last_volume);
				if(last_volume == 0){
					$('#' + id['controls'] + '_audio').addClass('icon-volume-stash-solid').removeClass('icon-volume-mute-solid icon-volume-down-solid icon-volume-solid icon-volume-up-solid');
				}else if((last_volume > 0) && (last_volume <= 0.3)){
					$('#' + id['controls'] + '_audio').addClass('icon-volume-down-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-solid icon-volume-up-solid');
				}else if((last_volume > 0.3) && (last_volume <= 0.7)){
					$('#' + id['controls'] + '_audio').addClass('icon-volume-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-down-solid icon-volume-up-solid');
				}else if((last_volume > 0.7) && (last_volume <= 1)){
					$('#' + id['controls'] + '_audio').addClass('icon-volume-up-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-down-solid icon-volume-solid');
				}
				$('#'+id['player']).removeAttr('muted');
				$('#'+id['audio']).removeAttr('muted');
				mute = false;
			}
		});

// 		AUDIOBAR + MUTE BUTTON
		$('#' + id['controls'] + '_audiobar').on('change', function(){
			var volume = $('#' + id['controls'] + '_audiobar').val();
			$('#'+id['player']).get(0).volume = volume;
			$('#'+id['audio']).get(0).volume = volume;
			if(volume == 0){
				$('#' + id['controls'] + '_audio').addClass('icon-volume-stash-solid').removeClass('icon-volume-mute-solid icon-volume-down-solid icon-volume-solid icon-volume-up-solid');
			}else if((volume > 0) && (volume <= 0.3)){
				$('#' + id['controls'] + '_audio').addClass('icon-volume-down-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-solid icon-volume-up-solid');
			}else if((volume > 0.3) && (volume <= 0.7)){
				$('#' + id['controls'] + '_audio').addClass('icon-volume-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-down-solid icon-volume-up-solid');
			}else if((volume > 0.7) && (volume <= 1)){
				$('#' + id['controls'] + '_audio').addClass('icon-volume-up-solid').removeClass('icon-volume-mute-solid icon-volume-stash-solid icon-volume-down-solid icon-volume-solid');
			}
			
			if(mute === true){
				$('#'+id['player']).removeAttr('muted');
				$('#'+id['audio']).removeAttr('muted');
				mute = false;
			}
			last_volume = volume;
		});

// 		FULLSCREEN
		$('#' + id['controls'] + '_fullscreen').on('click', function(){
			if (screenfull.isEnabled) {
				screenfull.toggle($('#player').get(0));
			}
		});
		if(screenfull.isEnabled) {
			screenfull.on('change', function(){
				if(screenfull.isFullscreen){
					$('#'+id['container']).addClass('player_fullscreen');
					$('#'+id['container']).removeAttr('style');
					$('#'+id['player']).addClass('video_fullscreen');
					$('#' + id['controls'] + '_fullscreen').removeClass('icon-expand-alt-solid').addClass('icon-compress-alt-solid');
					control_width(false, initial_range_width);
					//give container the videos width as max width to center the video element and prevent vessels from floating out of the container
					let videoWidth = $('#'+id['player']).width();
					$('#'+id['container']).width(videoWidth);
				}else{
					$('#'+id['container']).removeClass('player_fullscreen');
					$('#'+id['container']).height(settings['options']['height']);
					$('#'+id['container']).width(settings['options']['width']);
					$('#'+id['player']).removeClass('video_fullscreen');
					$('#' + id['controls'] + '_fullscreen').removeClass('icon-compress-alt-solid').addClass('icon-expand-alt-solid');
					control_width(true, initial_range_width);
				}
			});
		}

		//CUEPOINTS
		const nextCue = $('#'+id['controls'] + '_next_cue');
		const prevCue = $('#'+id['controls'] + '_prev_cue');
		var jiavp_cue_vid = {};
		if(settings['cuepoints'] && settings['options']['cuejump']){
			$.each(settings['cuepoints'], function(i){
				jiavp_cue_vid[i] = [];
			});
			$.each(settings['cuepoints'], function(){
				var splitTime = this['time'].split(':');
				var cueTime   =	(+splitTime[0]) * 60 * 60 + (+splitTime[1]) * 60 + (+splitTime[2]);
				if(isDefined(jiavp_cue_vid[this['video']])){
					jiavp_cue_vid[this['video']].push(cueTime);
				}
			});
			nextCue.on('click', cueController());
			prevCue.on('click', cueController(true));
			
			function cueController(prev = false){
				$.each(settings['cuepoints'], function(){
					if(isDefined(jiavp_cue_vid[this['video']])){
						jiavp_cue_vid[this['video']].sort(function(a, b){return (prev ? b-a : a-b);});
					}
				});
				
				
				let lengthComparer	=	jiavp_cue_vid[currentVideo][jiavp_cue_vid[currentVideo].length - 1] + (prev ? 1 : 0);
				let timeComparer	=	$('#'+id['player']).get(0).currentTime;
				let comparison		=	(prev ? lengthComparer < timeComparer : lengthComparer > timeComparer);
				
				if(jiavp_cue_vid[currentVideo].length > 0 && comparison){
					$.each(jiavp_cue_vid[currentVideo], function(index){
						let videoTime		=	$('#'+id['player']).get(0).currentTime;
						let indexedVideo	=	jiavp_cue_vid[currentVideo][index];
						
						let timeComparison	=	(prev ? videoTime > indexedVideo : videoTime < indexedVideo);
						if(timeComparison){
							videoTime = indexedVideo;
							$('#'+id['player']).trigger('timeupdate');
							return false;
						}
					});
				}else{
					var nextVideo = currentVideo + (prev ? -1 : 1);
					let inRange = (prev ? nextVideo >= 0 : true);
					var tt = false;
					if(inRange && tt) {
						for(var i = nextVideo; i < Object.keys(settings['videos']).length;){
							if(jiavp_cue_vid[i].length > 0){
								$(document).trigger('clipJump', nextVideo);
								$('#'+id['player']).get(0).currentTime = jiavp_cue_vid[i][0];
								$('#'+id['player']).trigger('timeupdate');
								return false;
							}
							if(prev){
								i--;
							}else{
								i++;
							}
						}
					}
				}
			}
		}
		
		
		if (settings['options']['startbutton']){
			$('#'+id['player']).removeAttr('autoplay');
			$('#'+id['autio']).removeAttr('autoplay');
			//$('#'+id['player']).css('display', 'none');
			let startbutton = `<span id="${id['startbutton']}" class="icon-play-circle-regular startbutton"></span>`;
			$('#'+id['container']).prepend(startbutton);
			$('#' + id['startbutton']).on('click', () => {
				//$('#'+id['player']).removeAttr('style');
				$('#' + id['startbutton']).remove();
				$('#' + id['player']).get(0).play();
				if (isDefined(settings['audio'])){
					$('#' + id['audio']).get(0).play();
				}
			});
		}
		return this;
	};
}(jQuery));
