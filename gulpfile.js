const gulp 			= require('gulp');
const autoprefixer 	= require('gulp-autoprefixer');
const sass 			= require('gulp-sass');
const terser 		= require('gulp-terser');
const sourcemaps 	= require('gulp-sourcemaps');

exports.default = function(cb) {
	gulp.series(styles, scripts);
	gulp.watch('source/scripts/*.js', scripts());
	gulp.watch('source/styles/**/*.scss', styles());
	cb();
}

exports.scripts = function(cb) {
	scripts();
	cb();
}

exports.styles = function(cb){
	styles();
	cb();
}

function scripts() {
	return gulp.src('source/scripts/*.js')
		.pipe(sourcemaps.init())
		.pipe(terser())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('robert/js'))
		.pipe(gulp.dest('build/js'));
}

function styles() {
	return gulp.src('source/styles/jIAVP.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
            cascade: false,
            grid: "autoplace"
        }))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('robert/css'))
		.pipe(gulp.dest('build/css'));
}